#!/bin/bash -ex

echo $0

echo "Sending kill signal to transmission-daemon"
PID=$(pidof transmission-daemon)
if [[ -n "$PID" ]]; then
    kill "$PID"
fi

# Give transmission-daemon some time to shut down
TRANSMISSION_TIMEOUT_SEC=${TRANSMISSION_TIMEOUT_SEC:-5}
for i in $(seq "$TRANSMISSION_TIMEOUT_SEC")
do
    sleep 1
    [[ -z "$(pidof transmission-daemon)" ]] && break
    [[ $i == 1 ]] && echo "Waiting ${TRANSMISSION_TIMEOUT_SEC}s for transmission-daemon to die"
done

# Check whether transmission-daemon is still running
if [[ -z "$(pidof transmission-daemon)" ]]
then
    echo "Successfuly closed transmission-daemon"
else
    echo "Sending kill signal (SIGKILL) to transmission-daemon"
    kill -9 "$PID"
fi
